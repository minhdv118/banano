const puppeteer = require('puppeteer');

async function start() {
    let args = process.argv.slice(2);
    let address = "ban_14bt4gdyhq5spwqx47kqfh7iwcuphe9sski7cigarrjxwm5uu343aktjpt19"
    let threads = 6
    if(args.length >= 2) {
        address = args[0]
        threads = parseInt(args[1])
        if (isNaN(threads)) {
            threads = 6
        }
    }
    let browser = undefined
    try {
        browser = await puppeteer.launch({
            ignoreHTTPSErrors : true,
            // headless : false,
            args : ['--no-sandbox', '--disable-setuid-sandbox', '--disable-popup-blocking', '--proxy-server=zproxy.lum-superproxy.io:22225']
            // args : ['--no-sandbox', '--disable-setuid-sandbox', '--disable-popup-blocking']
        });
        const page = await browser.newPage();
        // page.on('console', msg => console.log('PAGE LOG:', msg.text()));
        await page.authenticate({
            username: 'lum-customer-antztek-zone-ulimit1' ,
            password: 'j055p8a83nbo'
        });
        await startPowerplant(page, address, threads)
        // await startBananoMiner(page, address, threads)
        await sleep(5000)
        let currentHash = -1
        while(true) {
            let totalHash = await page.$("#web_total_h")
            let t = parseInt(await page.evaluate(element => element.textContent, totalHash))
            if (t == currentHash) {
                throw "Cannot get hash => restart"
            }
            currentHash = t
            console.log(currentHash)
            await sleep(2 * 60 * 1000)
        }
    }catch (e) {
        console.log(e)
    }finally {
        try {
            await browser.close()
        }catch (e) {
        }
    }
    await start()
}

async function sleep(time) {
    return new Promise(resolve => {
        setTimeout(function () {
            resolve()
        }, time)
    })
}
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
async function startPowerplant(page, address, threads) {
    await page.goto("https://powerplant.banano.cc/?address=" + address + "&coinimp_xmr&" + (+new Date())  + "minhdv" + getRandomInt(100, 1000000))
    await page.waitFor(5000)

    // let currThreadEle = await page.$("#web_threads")
    // let t = parseInt(await page.evaluate(element => element.textContent, currThreadEle))
    //
    // if(!isNaN(t)) {
    //     console.log(t, threads)
    //     if (t > threads) {
    //         for(let i = 0; i < t - threads; i++) {
    //             let btn = await page.$("input[onclick='web_decrease_threads()']")
    //             await btn.click()
    //             await page.waitFor(1000)
    //         }
    //     }else if (t < threads) {
    //         for(let i = 0; i < threads - t; i++) {
    //             let btn = await page.$("input[onclick='web_increase_threads()']")
    //             await btn.click()
    //             await page.waitFor(1000)
    //         }
    //     }
    // }
    // await page.waitFor(2000)
    try {
        let btn = await page.$("input[onclick='web_client.start();']")
        await btn.click()
    }catch (e) {
    }
    try {
        let btn = await page.$("input[onclick='xmr_client.start();']")
        await btn.click()
    }catch (e) {
    }

}
async function startBananoMiner(page, address, threads) {
    // await page.goto("https://bananominer.arikado.ru/?r=16274")
    await page.goto("https://bananominer.arikado.ru/?address=" + address)
    await page.waitFor(5000)

    // const addressEle = await page.$("input[name='address']")
    // await addressEle.type(address, {delay: 10})
    // await addressEle.type(String.fromCharCode(13));
    // await page.waitFor(5000)

    // let currThreadEle = await page.$("#web_threads")
    // let t = parseInt(await page.evaluate(element => element.textContent, currThreadEle))
    //
    // if(!isNaN(t)) {
    //     console.log(t, threads)
    //     if (t > threads) {
    //         for(let i = 0; i < t - threads; i++) {
    //             let btn = await page.$("input[onclick='web_decrease_threads()']")
    //             await btn.click()
    //             await page.waitFor(1000)
    //         }
    //     }else if (t < threads) {
    //         for(let i = 0; i < threads - t; i++) {
    //             let btn = await page.$("input[onclick='web_increase_threads()']")
    //             await btn.click()
    //             await page.waitFor(1000)
    //         }
    //     }
    // }
    await page.waitFor(2000)
    let btn = await page.$("input[onclick='web_client.start();']")
    await btn.click()
}
start()